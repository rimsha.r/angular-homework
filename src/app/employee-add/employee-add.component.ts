import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {DataService} from "../data.service";
import {Employee} from "../entities/employee";

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.css']
})
export class EmployeeAddComponent implements OnInit {

  form: FormGroup;


  constructor(private formBuilder: FormBuilder, public dataService: DataService) {
    this.form = formBuilder.group({
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      email: new FormControl(''),
      departmentId: new FormControl(''),
      positionId: new FormControl(''),
    })
  }

  ngOnInit(): void {
  }

  add() {
    const firstName = this.form.get('firstName').value;
    const lastName = this.form.get('lastName').value;
    const email = this.form.get('email').value;
    const departmentId = this.form.get('departmentId').value;
    const positionId = this.form.get('positionId').value;
    if (!firstName || !lastName || !email) {
      return;
    }
    // @ts-ignore
    this.dataService.addEmployee({
      firstName: firstName,
      lastName: lastName,
      email: email,
      departmentId: departmentId,
      positionId: positionId
    })
      .subscribe(employee => {
        this.dataService.employees.push(employee);
      });
  }

  close() {
    document.getElementById("add").style.display = "none";
  }



}
