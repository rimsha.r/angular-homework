import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Department} from "./entities/department";
import {Observable, of} from "rxjs";
import {Employee} from "./entities/employee";
import {Position} from "./entities/position";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private deptUrl = 'http://localhost:8080/departments';
  private employeeUrl = 'http://localhost:8080/employees';
  private positionUrl = 'http://localhost:8080/positions';

  employees: Employee[];
  positions: Position[];
  departments: Department[];


  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  departmentId: number;
  positionId: number;
  employee: Employee = new class implements Employee {
    departmentId: number;
    email: string;
    firstName: string;
    id: number;
    lastName: string;
    positionId: number;
  };

  constructor(private http: HttpClient) { }

  getDepartments(): Observable<Department[]> {
    return this.http.get<Department[]>(this.deptUrl);
  }

  getPositions(): Observable<Position[]> {
    return this.http.get<Position[]>(this.positionUrl);
  }

  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.employeeUrl);
  }

  getDepartment(id: number): Observable<Department> {
    return this.http.get<Department>(this.deptUrl + '/' + id);
  }

  getPosition(id: number): Observable<Position> {
    return this.http.get<Position>(this.positionUrl + '/' + id);
  }

  getEmployee(id: number): Observable<Employee> {
    return this.http.get<Employee>(this.employeeUrl + '/' + id);
  }

  addPosition(position: Position): Observable<Position> {
    return this.http.put<Position>(this.positionUrl, position, this.httpOptions);
  }

  addDepartment(department: Department): Observable<Department> {
    return this.http.put<Department>(this.deptUrl, department, this.httpOptions);
  }

  addEmployee(employee: Employee): Observable<Employee> {
    return this.http.put<Employee>(this.employeeUrl, employee, this.httpOptions);
  }


  deleteDept(id: number): Observable<any> {
      return this.http.delete(this.deptUrl + '/' + id);
  }

  deleteEmployee(id: number) {
    return this.http.delete(this.employeeUrl + '/' + id);
  }

  deletePosition(id: number) {
    return this.http.delete(this.positionUrl + '/' + id);
  }

  saveDepartment(department: Department) {
    return this.http.patch(this.deptUrl, department, this.httpOptions).subscribe();
  }

  savePosition(position: Position) {
    return this.http.patch(this.positionUrl, position, this.httpOptions).subscribe();
  }

  editEmployee(employee: Employee){
    return this.http.patch(this.employeeUrl, employee, this.httpOptions).subscribe();
  }
}
