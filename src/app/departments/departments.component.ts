import { Component, OnInit } from '@angular/core';
import {DataService} from "../data.service";
import {Department} from "../entities/department";

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent implements OnInit {
  private department: Department;


  constructor(public dataService: DataService) { }

  ngOnInit(): void {
    this.getDepartments();
  }

  getDepartments(): void {
    this.dataService.getDepartments().subscribe(departments => this.dataService.departments = departments);
  }

  add(name: string) {
    name = name.trim();
    if (!name) { return; }
    // @ts-ignore
    this.dataService.addDepartment({title: name})
      .subscribe(department => {
        this.dataService.departments.push(department);
      });
  }

  //////////////tried to make entries editable directly in table - didn't work
  // edit(department: Department) {
  //   const name = document.getElementsByClassName('dept' + department.id)[0].getElementsByClassName("name");
  //   name[0].setAttribute('contenteditable', 'true');
  //   const button = document.getElementsByClassName('dept' + department.id)[0].getElementsByClassName("button");
  //   button[0].innerHTML = "Save";
  //   button[0].setAttribute('(click)', 'save(department)');
  // }


  delete(department: Department) {
    this.dataService.deleteDept(department.id).subscribe(result => {
      this.dataService.departments = this.dataService.departments.filter(item => item.id !== department.id);
    });
  }

  edit(department: Department) {
    this.dataService.departmentId = department.id;
    document.getElementById("department").style.display = "block";
  }

  save(value: string) {
    this.dataService.getDepartment(this.dataService.departmentId).subscribe(data => {
      this.department = data;
      this.department.title = value;
      this.dataService.saveDepartment(this.department);
      this.dataService.getDepartments().subscribe(data1 => this.dataService.departments = data1);
    });
    this.dataService.departmentId = null;
    this.department = null;
  }
  //
  cancel() {
    this.dataService.departmentId = null;
    document.getElementById("department").style.display = "none";
  }

  hide(){
    document.getElementById("department").style.display = "block";
  }
}
