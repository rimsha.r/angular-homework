import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, FormsModule} from "@angular/forms";
import {DataService} from "../data.service";
import {Employee} from "../entities/employee";

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})
export class EmployeeEditComponent implements OnInit {

  editForm: FormGroup;
  private employee: Employee;

  constructor(private formBuilder: FormBuilder, public dataService: DataService) {
    this.editForm = formBuilder.group({
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      email: new FormControl(''),
      departmentId: new FormControl(''),
      positionId: new FormControl(''),
    })
  }

  ngOnInit(): void {
  }

  close() {
    document.getElementById("edit").style.display = "none";
  }

  edit() {
    this.employee = this.editForm.getRawValue();
    if (this.employee.firstName != '') {
      this.dataService.employee.firstName = this.employee.firstName;
    }
    if (this.employee.lastName != '') {
      this.dataService.employee.lastName = this.employee.lastName;
    }
    if (this.employee.email != '') {
      this.dataService.employee.email = this.employee.email;
    }
    // @ts-ignore
    if (this.employee.departmentId != '') {
      this.dataService.employee.departmentId = this.employee.departmentId;
    }
    // @ts-ignore
    if (this.employee.positionId != '') {
      this.dataService.employee.positionId = this.employee.positionId;
    }

    this.employee = this.dataService.employee;
    console.log('this employee id is ' + this.employee.id + 'this employee name is ' + this.employee.firstName);

    this.dataService.editEmployee(this.employee);

    this.dataService.getEmployees().subscribe(data1 =>
      this.dataService.employees = data1
    );
  }

}
