import { Component, OnInit } from '@angular/core';
import {Employee} from "../entities/employee";
import {DataService} from "../data.service";


@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {


  constructor(public dataService: DataService) { }

  ngOnInit(): void {
    this.getEmployees();
  }

  getEmployees(): void {
    this.dataService.getEmployees().subscribe(employees => this.dataService.employees = employees);
  }

  // NO IDEA GOW TO GET NAMES
  // getDepartment(id: number): string {
  //   this.department = this.dataService.getDepartment(id).subscribe(dept => this.department = dept);
  //   const name =
  // }
  //
  // getPosition(id: number): string {
  //   this.dataService.getPosition(id).subscribe(position => this.position = position);
  //   return this.position.title;
  // }


  edit(employee: Employee) {
    this.dataService.employee = employee;
    console.log('dataservice employee after edit id:' + this.dataService.employee.id + ' name: ' + this.dataService.employee.firstName);
  }

  delete(employee: Employee) {
    this.dataService.deleteEmployee(employee.id).subscribe(result => {
      this.dataService.employees = this.dataService.employees.filter(item => item.id !== employee.id);
    });
  }

  addEmployee() {
    document.getElementById("add").style.display = "block";
  }

  show(){
    document.getElementById("edit").style.display = "block";
  }
}
